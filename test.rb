require 'imgkit'

File.open('testimg.png', 'wb') do |f|
  img = IMGKit.new(File.read("index.html"), "crop-w" => 1200 )
  img.stylesheets << "css/oversumo.css"
  f.write img.to_img
end
